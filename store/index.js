export const state = () => ({
  email: '',
  password: '',
  username: '',
  accessToken: '',
})

export const mutations = {
  updateEmail(state, email) {
    state.email = email
  },
  clearEmail(state) {
    state.email = ''
  },
  updatePassword(state, password) {
    state.password = password
  },
  clearPassword(state) {
    state.password = ''
  },
  updateUsername(state, username) {
    state.username = username
  },
  clearUsername(state) {
    state.username = ''
  },
  updateToken(state, token) {
    state.accessToken = token
  },
  removeToken(state) {
    state.accessToken = ''
  },
}
